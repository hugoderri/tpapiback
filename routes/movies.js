var express = require('express');
var router = express.Router();

const _ = require('lodash');

let movies = [
  {
  movie: "La La Land",
  year: "2016",
  id: "0"},
  {
  movie: "Les 3 Frères",
  year: "1999",
  id:"1"},   
  {
  movie: "Fast and Furious",
  year: "2016",
  id:"2"},
  {
  movie: "Taken",
  year: "2010",
  id:"3"},  
];

/* GET users listing. */
router.get('/', (req, res) => {
  // Get List of user and return JSON
  res.status(200).json({ movies });
});

/* GET one user. */
router.get('/:id', (req, res) => {
  const { id } = req.params;
  // Find user in DB
  const movie = _.find(movies, ["id", id]);
  // Return user
  res.status(200).json({
    message: 'User found!',
    movie 
  });
});

/* PUT new user. */
router.put('/', (req, res) => {
  // Get the data from request from request
  const { movie } = req.body;
  // Create new unique id
  const id = _.uniqueId();
  // Insert it in array (normaly with connect the data with the database)
  users.push({ movie, id });
  // Return message
  res.json({
    message: `Just added ${id}`,
    movie: { movie, id }
  });
});

/* UPDATE user. */
router.post('/:id', (req, res) => {
  // Get the :id of the user we want to update from the params of the request
  const { id } = req.params;
  // Get the new data of the user we want to update from the body of the request
  const { movie } = req.body;
  // Find in DB
  const userToUpdate = _.find(movies, ["id", id]);
  // Update data with new data (js is by address)
  userToUpdate.movie = movie;

  // Return message
  res.json({
    message: `Just updated ${id} with ${movie}`
  });
});



module.exports = router;
